![Python version](https://img.shields.io/pypi/pyversions/Django.svg?style=for-the-badge)

# Vibrato
![Alt text](vibrationsCarto.png "Cartographie des vibrations émisent")

Le projet Vibrato a pour but de permettre la détection des anomalies de voie à partir de signaux fournis par des accéléromètres embarqués.

## Requirements
- [Python 3](https://www.python.org/downloads/)
- [Emacs 25+](https://www.gnu.org/software/emacs/)

## Génération des figures et du rapport
```bash
git clone git@gitlab.com:sncf_fbd/vibrato.git .
cd vibrato/report
make
```
